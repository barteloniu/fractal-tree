import { Tree } from "./tree";

const ctx = (document.querySelector("#canvas") as HTMLCanvasElement).getContext("2d") as CanvasRenderingContext2D
const sizeInput = document.querySelector("#sizeInput") as HTMLInputElement
const anlgeInput = document.querySelector("#angleInput") as HTMLInputElement
const randomnessInput = document.querySelector("#randomnessInput") as HTMLInputElement
anlgeInput.max = Math.PI.toString()
anlgeInput.value = (Math.PI / 6).toString()

ctx.strokeStyle = "#ffccff"
ctx.lineWidth = 2

const tree = new Tree(
    parseInt(sizeInput.value),
    parseFloat(anlgeInput.value),
    parseFloat(randomnessInput.value)
)
tree.grow(ctx)

const redraw = () => {
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
    tree.grow(ctx)
}

sizeInput.addEventListener("input", e => {
    tree.size = parseInt(sizeInput.value)
    redraw()
})
anlgeInput.addEventListener("input", e => {
    tree.angle = parseFloat(anlgeInput.value)
    redraw()
})
randomnessInput.addEventListener("input", e => {
    tree.randomness = parseFloat(randomnessInput.value)
    redraw()
})
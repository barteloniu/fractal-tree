export class Vector{

    x: number
    y: number

    constructor(x: number, y: number){
        this.x = x
        this.y = y
    }

    distance(v: Vector): number{
        const a = this.x - v.x
        const b = this.y - v.y
        return Math.sqrt(a ** 2 + b ** 2)
    }

    angle(v: Vector): number{
        const x = this.x - v.x
        const y = this.y - v.y
        return Math.atan2(y, x)
    }

}
import { Vector } from "./vector";

export class Branch{

    start: Vector
    end: Vector
    angle: number
    randomness: number

    constructor(start: Vector, end: Vector, angle: number, randomness: number){
        this.start = start
        this.end = end
        this.angle = angle
        this.randomness = randomness
    }

    show(ctx: CanvasRenderingContext2D): void{
        ctx.beginPath()
        ctx.moveTo(this.start.x, this.start.y)
        ctx.lineTo(this.end.x, this.end.y)
        ctx.stroke()
    }

    branch(): Branch[]{
        const len = this.start.distance(this.end) * 2 / 3
        const newAngle = this.start.angle(this.end)

        const leftA = newAngle - this.angle * (1 + Math.random() * this.randomness - this.randomness / 2)
        const leftE = new Vector(
            this.end.x - Math.cos(leftA) * len,
            this.end.y - Math.sin(leftA) * len
        )

        const rightA = newAngle + this.angle * (1 + Math.random() * this.randomness - this.randomness / 2)
        const rightE = new Vector(
            this.end.x - Math.cos(rightA) * len,
            this.end.y - Math.sin(rightA) * len
        )

        return [new Branch(this.end, leftE, this.angle, this.randomness), new Branch(this.end, rightE, this.angle, this.randomness)]
    }

}
import { Branch } from "./branch";
import { Vector } from "./vector";

export class Tree{

    branches: Branch[] = []
    size: number
    angle: number
    randomness: number

    constructor(size: number, angle: number, randomness: number){
        this.size = size
        this.angle = angle
        this.randomness = randomness
    }

    grow(ctx: CanvasRenderingContext2D){
        this.branches = [
            new Branch(
                new Vector(ctx.canvas.width / 2, ctx.canvas.height),
                new Vector(ctx.canvas.width / 2, ctx.canvas.height - 180),
                this.angle,
                this.randomness
            )
        ]
        let treeTop = this.branches.slice()
        for(let i = 0; i < this.size; i++){
            let newTop: Branch[] = []
            for(const b of treeTop){
                newTop.push(...b.branch())
            }
            treeTop = newTop.slice()
            this.branches = this.branches.concat(newTop)
        }
        for(const b of this.branches){
            b.show(ctx)
        }
    }

}
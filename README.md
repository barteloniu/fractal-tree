# fractal-tree

A fractal tree renderer in TypeScript  
Play it at [barteloniu.gitlab.io/fractal-tree](https://barteloniu.gitlab.io/fractal-tree)

Inspired by [The Coding Train](https://www.youtube.com/channel/UCvjgXvBlbQiydffZU7m1_aw)